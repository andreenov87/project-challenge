import React from "react";
import "../style.css";

const TextInput = (props) => {
  const { value, disabled, onChange } = props;

  return (
    <input
      className="text-style"
      type="text"
      value={value}
      disabled={disabled}
      onChange={onChange}
    />
  );
};

export default TextInput;

import React, { useState } from "react";
import "./style.css";
import TextInput from "./component/TextInput";
import Label from "./component/Label";
function ProjectChallenge() {
  const [isValue, setIsValue] = useState(0);

  const handleChange = (value) => {
    setIsValue(value.target.value * 2);
  };

  return (
    <div className="main-content">
      <div className="title">
        <h3>Front end Code Challengeeeeee</h3>
        <h3>Front end Code Challengeeeeee</h3>
        <h3>Front end Code Challengeeeeee</h3>
      </div>
      <div className="box-style">
        <div className="input-div">
          <Label name="Input" />
          <TextInput disabled={false} onChange={handleChange} />
        </div>
      </div>
      <div className="box-style">
        <div className="input-div">
          <Label name="Output" />
          <TextInput value={isValue} disabled={true} />
        </div>
      </div>
    </div>
  );
}

export default ProjectChallenge;

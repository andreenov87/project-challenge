import "./App.css";
import ProjectChallenge from "./ProjectChallenge";

function App() {
  return (
    <div className="App">
      <ProjectChallenge />
    </div>
  );
}

export default App;
